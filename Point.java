public class Point 
{
    private int x,y;

    public Point (int xval, int yval)
    {
        x = xval;
        y = yval;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    /* 
       shift: return a _new_ Point object with the 
       x and y values adjusted accoring to the given direction
    */
    public Point shift (Compass direction)
    {
        /* you write this */
        return new Point(-1,-2); // this is wrong but lets it compile
    }

}


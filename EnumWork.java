// Once you are done this test should work with EnumWork.testit() 

public class EnumWork
{
    public static void testit() {
        Point a = new Point(1,4);
        Compass dir = Compass.North;
        Point b = a.shift( dir );
        System.out.println("Original: "+a);
        System.out.println("Shifted "+dir+" becomes "+b);
    }

}

